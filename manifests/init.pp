#
# Installs mod_jk.

class mod_jk {
    case $::operatingsystem {
        'debian', 'ubuntu': {
            package { 'libapache2-mod-jk':
                alias  => 'mod_jk',
                ensure => present;
            }
        }
        'redhat': {
            case $::lsbdistrelease {
                # there is no mod_jk for RHEL3 or RHEL4
                '3', '4': { }
                # RHEL5
                default: {
                    package { 'mod_jk': ensure => present }
                }
            }
        }
    }
}
